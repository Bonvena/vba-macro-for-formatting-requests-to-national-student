# README #

A macro is well-suited for NSC StudentTracker requests due to the fixed format requested. 
The attached macro-enabled Excel workbook will, once imported, format the necessary data elements
(first name, middle name, last name, date of birth, and ID) into a query acceptable to NSC.

It will also provide the number of cases that fall under several unique scenarios, such as suffixes or characters
that will cause NSC to report an error. Suffixes (such as Jr. and III) are usually found after the last name
when they may be better off in the suffix field. Characters that cause errors should be removed, such as most periods. 
Periods after “St” appear to be fine (for now).

Address locations are provided for two types of errors that are more difficult to locate. 
Other errors can be found via the Find tool. Note that first name, middle name, last name, date of birth (in YYYYMMDD format), and ID must be in found in this order between columns A and E, with headings in the first row. That means the first name of the first student will be found in A2, the middle name in B2, the last name in C2, DOB in D2, and ID in E2.

Note: you will have to run this code in Excel’s Visual Basic Editor, which requires adding the Developer tab to Excel.
This can be done via Options -> Customize Ribbon.

This code is licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license.
Contact me for commercial uses.